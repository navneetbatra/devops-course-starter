# DevOps Apprenticeship: Project Exercise

## Overview

The project exercise is  a Todo List application which manages the task list using external api trello.

### Context Diagram

![](</documentation/ToDoApp_Architecture-Context Diagram.drawio.png>)
### Container Diagram

![](</documentation/ToDoApp_Architecture-Container Diagram.drawio.png>)

### Component Diagram

![](</documentation/ToDoApp_Architecture-Component Diagram.drawio.png>)

## System Requirements

The project uses poetry for Python to create an isolated environment and manage package dependencies. To prepare your system, ensure you have an official distribution of Python version 3.7+ and install Poetry using one of the following commands (as instructed by the [poetry documentation](https://python-poetry.org/docs/#system-requirements)):

### Poetry installation (Bash)

```bash
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/install-poetry.py | python -
```

### Poetry installation (PowerShell)

```powershell
(Invoke-WebRequest -Uri https://raw.githubusercontent.com/python-poetry/poetry/master/install-poetry.py -UseBasicParsing).Content | python -
```

## Dependencies

The project uses a virtual environment to isolate package dependencies. To create the virtual environment and install required packages, run the following from your preferred shell:

```bash
$ poetry install
```

We're going to be using Trello's API to fetch and save to-do tasks. In order to call their API, you need to first [create an account](https://trello.com/signup), then generate an API key and token by following the [instructions here](https://trello.com/app-key).

Create a new board in trello and keep the board id safe. The board should have 2 lists, TO Do and Done.

All of these API calls require an API key and token to authenticate the request. These credentials are tied to your account and need to be kept secret! For this reason, they shouldn't be committed to your project's Git repository. There are a number of ways of specifying these credentials separately and then having the app read them in, such as using a separate configuration file or specifying environment variables. The project is already set up with a .env file, so add the API key and token as new variables in there.

You'll also need to clone a new `.env` file from the `.env.template` to store local configuration options. This is a one-time operation on first setup:

Update the values of BASE_URL, API_KEY, TOKEN and BOARD_ID in `.env` file.


```bash
$ cp .env.template .env  # (first time only)
```

The `.env` file is used by flask to set environment variables when running `flask run`. This enables things like development mode (which also enables features like hot reloading when you make a file change). There's also a [SECRET_KEY](https://flask.palletsprojects.com/en/1.1.x/config/#SECRET_KEY) variable which is used to encrypt the flask session cookie.

## Running the App

Once the all dependencies have been installed, start the Flask app in development mode within the Poetry environment by running:
```bash
$ poetry run flask run
```

You should see output similar to the following:
```bash
 * Serving Flask app "app" (lazy loading)
 * Environment: development
 * Debug mode: on
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
 * Restarting with fsevents reloader
 * Debugger is active!
 * Debugger PIN: 226-556-590
```
Now visit [`http://localhost:5000/`](http://localhost:5000/) in your web browser to view the app.

## Running Tests
Test modules are placed under the tests directory. Note that tests is not a Python package and has no "__init__.py" file.
pytest has many command line options with a powerful discovery mechanism:

```bash
poetry run pytest to discover and run all tests from the current directory
poetry run pytest -v to explicitly print the result of each test as it is run
poetry run pytest tests/test_viewmodel.py to run only the view model function tests
poetry run pytest tests/test_viewmodel.py::test_doing_items to run only test method 'test_doing_items'

poetry run pytest tests/test_integration.py to run only the integration tests

poetry run pytest --junitxml=results.xml to generate a JUnit-style XML test report
poetry run pytest -h for command line help
```

It is also possible to run pytest directly with the "pytest" or "py.test" command, instead of using the longer "python -m pytest" module form. However, the shorter command does not append the current directory path to PYTHONPATH.

# To provision a virtual machine using ansible:
Go to controller machine haivng ansible installed
Execute: ansible-playbook playbook.yaml -i inventory 

# To provision a build using Docker:
## Step 1: Install Docker
If you haven’t already, you’ll need to install Docker Desktop. Installation instructions for Windows can be found [here](https://docs.docker.com/get-docker/). If prompted to choose between using Linux or Windows containers during setup, make sure you choose Linux containers.

## Step 2: Build docker image
You can build docker images for production or development using below mentioned commmands.
$ docker build --target development --tag todo-app:dev .
$ docker build --target production --tag todo-app:prod .

## Step 3: Running docker container
On UNIX shells, you can test the local development setup using a command similar to:
docker run --env-file .env -it --publish 8888:80 --mount type=bind,source="$(pwd)"/todo_app,target=/app/todo_app todo-app:dev
You can also use the -d flag to detach from the docker shell. This will launch your container in the background. To view the container logs, you’ll need to use docker logs <CONTAINER>

For production setup, you can use a command similar to:
docker run --env-file .env -d --publish 8000:8000 todo-app:prod

# To run test using Docker:
## Step 1: Build docker image
You can build docker images for tests using docker command.
# Build it
$ docker build --target test --tag my-test-image .

## Step 2: Run docker image
You can run docker images for running tests using docker command.
# Run tests in the "tests" directory
$ docker run my-test-image --env-file .env

# To provision a build using Docker Compose:
## Step 1: Build docker image
You can build docker images for production or development using docker compose.
$ docker-compose -f docker-compose-prod build
$ docker-compose -f docker-compose-prod.yml build

## Step 2: Run docker image
You can run docker images for production or development using docker compose.
$ docker-compose -f docker-compose-prod up
$ docker-compose -f docker-compose-prod.yml up

# Continous Deployment:
The production image is published to Docker Hub: navneetbatra/todoapp:latest as part of gitlab pipeline when all tests completes successfully. The pipeline triggers Azure webapp hook to upload latest release package and deploy to azure. The web app hosted on azure can be accessed at: https://navneetbatra-todoapp.azurewebsites.net/

