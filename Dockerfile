FROM python:3.10.11-slim-bullseye as base
RUN pip3 install poetry && pip install gunicorn
RUN mkdir -p /app
WORKDIR /app
COPY poetry.toml pyproject.toml /app/
RUN poetry install
COPY . /app/

FROM base as production
CMD ["poetry", "run", "gunicorn", "-b", "0.0.0.0:80", "todo_app.app:create_app()"]
EXPOSE 80

FROM base as development
ENV FLASK_ENV=development \
    FLASK_DEBUG=1 \
    FLASK_RUN_HOST=0.0.0.0 \
    FLASK_RUN_PORT=80
ENTRYPOINT ["poetry", "run"]
CMD ["poetry", "run", "flask", "run"]
EXPOSE 80

FROM base as debug

FROM base as test
ENV GECKODRIVER_VER v0.31.0
 # Install the long-term support version of Firefox (and curl if you don't have it already)
RUN apt-get update && apt-get install -y firefox-esr curl
  # Download geckodriver and put it in the usr/bin folder
RUN curl -sSLO https://github.com/mozilla/geckodriver/releases/download/${GECKODRIVER_VER}/geckodriver-${GECKODRIVER_VER}-linux64.tar.gz \
   && tar zxf geckodriver-*.tar.gz \
   && mv geckodriver /usr/bin/ \
   && rm geckodriver-*.tar.gz
ENTRYPOINT ["poetry", "run", "pytest", "--cov=todo_app", "--cov-report=xml:coverage/coverage.xml", "--cov-report=term"]
