from datetime import datetime

TODO_STATUS = 'To Do'
DONE_STATUS = 'Done'
DOING_STATUS = 'Doing'


class Item:
    def __init__(self, id, name, dateLastActivity=datetime.utcnow(), status=TODO_STATUS):
        self.id = id
        self.name = name
        self.dateLastActivity = dateLastActivity
        self.status = status

    @classmethod
    def from_json(cls, json):
        return cls(json['_id'], json['name'], json['dateLastActivity'], json['status'])