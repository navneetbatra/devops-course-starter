import datetime
from bson import ObjectId
import pymongo

from todo_app.data.Item import *

class MongoItems:
    def __init__(self, connection_string, database_name):
        self.client = pymongo.MongoClient(connection_string)
        self.db = self.client.get_database(database_name)

    def create_card(self, name):
        newtodo = {"name": name, "dateLastActivity": datetime.utcnow(),
                   "status": TODO_STATUS}
        self.db.todos.insert_one(newtodo)

    def get_all_items(self):
        saved_todos = self.db.todos.find()
        items = [Item.from_json(todo) for todo in saved_todos]
        return items

    def mark_complete(self, id):
        self.update_status(id, DONE_STATUS)

    def mark_todo(self, id):
        self.update_status(id, TODO_STATUS)

    def update_status(self, id, status):
        filter = {"_id": ObjectId(id)}
        newvalues = {"$set": {'status': status,
                              'dateLastActivity':  datetime.utcnow()}}
        self.db.todos.update_one(filter, newvalues)
