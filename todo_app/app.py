import os
from flask import Flask, render_template, request, redirect

from todo_app.flask_config import Config
from todo_app.data.MongoItems import *
from todo_app.data.ViewModel import *


def create_app():
    app = Flask(__name__)
    # We specify the full path and remove the import for this config so
    # it loads the env variables when the app is created, rather than when this file is imported

    app.config.from_object(Config())

    connection_string = os.getenv('CONNECTION_STRING')
    db_name = os.getenv('DATABASE_NAME')
    mongo_items = MongoItems(connection_string, db_name)

    @app.route('/')
    def index():
        items = mongo_items.get_all_items()
        items_view_model = ViewModel(items)
        return render_template('index.html', view_model=items_view_model)

    @app.route('/add', methods=['POST'])
    def add():
        itemTitle = request.form.get('itemtitle')
        mongo_items.create_card(itemTitle)
        return redirect('/')

    @app.route('/markcomplete/<string:id>')
    def markcomplete(id):
        mongo_items.mark_complete(id)
        return redirect('/')

    @app.route('/marktodo/<string:id>')
    def marktodo(id):
        mongo_items.mark_todo(id)
        return redirect('/')

    return app
