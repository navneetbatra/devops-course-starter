import pytest
from todo_app.data.ViewModel import *
from todo_app.data.Item import *

@pytest.fixture
def items():
    return [
                Item('1', "Module 1", datetime.time, 'Done'), 
                Item('2', "Module 2", datetime.time, 'Done'), 
                Item('3', "Module 3", datetime.time, 'Doing'),
                Item('4', "Module 4", datetime.time, 'To Do'), 
                Item('5', "Module 5", datetime.time, 'To Do'), 
                Item('6', "Module 6", datetime.time, 'To Do'),
                Item('7',"Module 7", datetime.time, 'To Do'), 
                Item('8', "Module 8", datetime.time, 'To Do'), 
                Item('9', "Module 9", datetime.time, 'To Do')
            ]

def test_doing_items(items):
    # Arrange
    view_model = ViewModel(items)

    # Assert
    assert 1 == len(view_model.doing_items)
    for item in view_model.doing_items:
        assert item.status == "Doing"

def test_todo_items(items):
    # Arrange
    view_model = ViewModel(items)

    # Assert
    assert 6 == len(view_model.todo_items)
    for item in view_model.todo_items:
        assert item.status == "To Do"

def test_done_items(items):
    # Arrange
    view_model = ViewModel(items)

    # Assert
    assert 2 == len(view_model.done_items)
    for item in view_model.done_items:
        assert item.status == "Done"
