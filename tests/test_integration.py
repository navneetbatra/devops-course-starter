import datetime
from bson import ObjectId
import pymongo
import pytest
from dotenv import load_dotenv, find_dotenv
from todo_app import app
import mongomock
from todo_app.data.Item import DOING_STATUS, DONE_STATUS, TODO_STATUS, Item


@pytest.fixture
def client():
    # Use our test integration config instead of the 'real' version
    file_path = find_dotenv('.env.test')
    load_dotenv(file_path, override=True)

    # Use the app to create a test_client that can be used in our tests.
    with mongomock.patch(servers=(('fakemongo.com', 27017),)):
        test_app = app.create_app()
        with test_app.test_client() as client:
            yield client


def test_index_page(client):
    # Setup test data
    setup_test_data("Test Todo")
    response = client.get('/')

    assert response.status_code == 200
    assert 'Test Todo' in response.data.decode()


def test_add_page(client):
    todoItem = {"itemtitle": "Post Todo"}
    response = client.post('/add', data=todoItem)

    assert response.status_code == 302
    response = client.get('/')
    assert response.status_code == 200
    assert 'Post Todo' in response.data.decode()


def test_markcomplete_page(client):
    todoItem = {"itemtitle": "MarkComplete"}
    response = client.post('/add', data=todoItem)
    id = get_id("MarkComplete")
    response = client.get('/markcomplete/' + str(id))

    assert response.status_code == 302
    response = client.get('/')
    assert response.status_code == 200
    assert 'Mark Done' not in response.data.decode()

def test_marktodo_page(client):
    todoItem = {"itemtitle": "MarkToDo"}
    response = client.post('/add', data=todoItem)
    id = get_id("MarkToDo")
    response = client.get('/marktodo/' + str(id))

    assert response.status_code == 302
    response = client.get('/')
    assert response.status_code == 200
    assert 'Mark Done' in response.data.decode()


def setup_test_data(name):
    todos = pymongo.MongoClient('mongodb://fakemongo.com').todo_db.todos
    newtodo = {"_id": ObjectId("64202334be34d56b3536b8b0"), "name": name, "dateLastActivity": datetime.datetime.utcnow(),
               "status": TODO_STATUS}
    todos.insert_one(newtodo)


def get_id(name):
    todos = pymongo.MongoClient('mongodb://fakemongo.com').todo_db.todos
    return todos.find_one({"name": name})["_id"]
