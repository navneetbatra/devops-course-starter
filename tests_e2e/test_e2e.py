import os
from typing import final
import certifi
import pymongo
import pytest
from threading import Thread
from time import sleep
import requests
from selenium import webdriver
from selenium.webdriver.common.by import By 
from dotenv import load_dotenv
from todo_app import app
from selenium.webdriver.common.keys import Keys


from selenium.webdriver.firefox.options import Options

@pytest.fixture(scope='module')
def driver():
    opts = Options()
    opts.headless = True
    with webdriver.Firefox(options=opts) as driver:
        yield driver
        
@pytest.fixture(scope='module')
def app_with_temp_db():
    # Load our real environment variables
    load_dotenv(override=True)

    os.environ['DATABASE_NAME'] = "temp_todo_db"

    # Construct the new application
    application = app.create_app()

    # Start the app in its own thread.
    thread = Thread(target=lambda: application.run(use_reloader=False))
    thread.daemon = True
    thread.start()
    
    # Give the app a moment to start
    sleep(1)

    # Return the application object as the result of the fixture
    yield application

    # Tear down
    thread.join(1)
    delete_temp_db("temp_todo_db")

def create_trello_board():
    url = os.environ['BASE_URL'] + "/boards/"
    
    params = {'key': os.environ['API_KEY'], 'token': os.environ['TOKEN'], "name": "E2E_Board"}
    response = requests.request("POST", url, params=params)
    print (response)
    board_id = response.json()["id"]
    return board_id

def delete_temp_db(database_name):
    ca = certifi.where()
    client = pymongo.MongoClient(os.environ["CONNECTION_STRING"], tlsCAFile=ca)
    client.drop_database(database_name)
    


def test_task_journey(driver, app_with_temp_db):
    driver.get('http://127.0.0.1:5000/')
    todo_items = driver.find_element(By.NAME, "todo_items").find_elements(By.TAG_NAME, "li")
    doing_items = driver.find_element(By.NAME, "doing_items").find_elements(By.TAG_NAME, "li")
    done_items = driver.find_element(By.NAME, "done_items").find_elements(By.TAG_NAME, "li")


    assert driver.title == 'To-Do App'
    assert 0 == len(todo_items)
    assert 0 == len(doing_items)
    assert 0 == len(done_items)

    newCardName = driver.find_element(By.NAME, "itemtitle")
    newCardName.send_keys("Module 1")
    add_item_button = driver.find_element(By.NAME, "additem")
    add_item_button.click()


    todo_items = driver.find_element(By.NAME, "todo_items").find_elements(By.TAG_NAME, "li")
    assert 1 == len(todo_items)

    newCardName = driver.find_element(By.NAME, "itemtitle")
    newCardName.send_keys("Module 2")
    add_item_button = driver.find_element(By.NAME, "additem")
    add_item_button.click()

    todo_items = driver.find_element(By.NAME, "todo_items").find_elements(By.TAG_NAME, "li")
    assert 2 == len(todo_items)
    done_items = driver.find_element(By.NAME, "done_items").find_elements(By.TAG_NAME, "li")
    assert 0 == len(done_items)

    element = driver.find_element(By.NAME, "Module 1")
    element.click()
    todo_items = driver.find_element(By.NAME, "todo_items").find_elements(By.TAG_NAME, "li")
    assert 1 == len(todo_items)
    done_items = driver.find_element(By.NAME, "done_items").find_elements(By.TAG_NAME, "li")
    assert 1 == len(done_items)
